<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 6</title>
    <style>
      table{
        border: 1px solid black;
        border-collapse: collapse;
      }
      tr,td{
        border: 1px solid black;
      }
    </style>
  </head>
  <body>

    <form name="loginForm" method="POST" action="login.php">
        <label for="usuario">Usuario</label>
        <input type="text" id="usuario" name="usuario">
        <br>
        <br>
        <label for="password">Contraseña</label>
        <input type="password" id="password" name="password">
        <br>
        <br>
        <button type="submit" >Iniciar sesión</button>
        <br>
    </form>

    <?php

    // $form = <<<EOD
    //
    // EOD;
    //
    // echo $form;
    //
    // require_once('login.php');
    require_once('conexion.php');


    $obj = new Conexion();
    $conexion = $obj->Conectar();

    $query = "select * from usuarios";
    $sql = $conexion->prepare($query);

    $sql->execute();

    $result = $sql->fetchAll();
    echo "<table>";
    echo "<tr>";
    echo "<td>";
    echo "Id";
    echo "</td>";
    echo "<td>";
    echo "usuario";
    echo "</td>";
    echo "<td>";
    echo "password";
    echo "</td>";
    echo "</tr>";
    foreach ($result as $row) {
      echo "<tr>";
      echo "<td>";
      echo $row["id"];
      echo "</td>";

      echo "<td>";
      echo $row["usuario"];
      echo "</td>";

      echo "<td>";
      echo $row["password"];
      echo "</td>";

      echo "</tr>";

    }
    echo "</table>";


    $obj=null;
     ?>
  </body>
</html>
