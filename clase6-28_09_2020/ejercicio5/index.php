<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 5</title>
    <style>
      table{
        border: 1px solid black;
        border-collapse: collapse;
      }
      tr,td{
        border: 1px solid black;
      }
    </style>
  </head>
  <body>
    <?php
    include('migrateDb.php');
    require_once('conexion.php');

    $migration = new Migration();

    $migration->populateRandomly();

    $obj = new Conexion();
    $conexion = $obj->Conectar();

    $query = "select * from ejercicio5table";
    $sql = $conexion->prepare($query);

    $sql->execute();

    $result = $sql->fetchAll();
    echo "<table>";
    echo "<tr>";
    echo "<td>";
    echo "Id";
    echo "</td>";
    echo "<td>";
    echo "Nombre";
    echo "</td>";
    echo "<td>";
    echo "Descripción";
    echo "</td>";
    echo "</tr>";
    foreach ($result as $row) {
      echo "<tr>";
      echo "<td>";
      echo $row["id"];
      echo "</td>";

      echo "<td>";
      echo $row["nombre"];
      echo "</td>";

      echo "<td>";
      echo $row["descripcion"];
      echo "</td>";

      echo "</tr>";

    }
    echo "</table>";


    $obj=null;
     ?>
  </body>
</html>
