<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 4</title>
    <style>
      table{
        border: 1px solid black;
        border-collapse: collapse;
      }
      tr,td{
        border: 1px solid black;
      }
    </style>
  </head>
  <body>
    <?php
    include('conexion.php');
    $obj = new Conexion();
    $conexion = $obj->Conectar();

    $query = "select p.id_producto as idproducto, p.nombre, p.precio, cat.nombre as categoria, m.nombre as marca, e.nombre as empresa
    from producto p
    join categoria cat on p.id_categoria = cat.id_categoria
    join marca m on m.id_marca = p.id_marca
    join empresa e on m.id_empresa = e.id_empresa";
    $sql = $conexion->prepare($query);

    $sql->execute();

    $result = $sql->fetchAll();
    echo "<table>";
    echo "<tr>";
    echo "<td>";
    echo "Nombre";
    echo "</td>";
    echo "<td>";
    echo "Precio";
    echo "</td>";
    echo "<td>";
    echo "Marca";
    echo "</td>";
    echo "<td>";
    echo "Empresa";
    echo "</td>";
    echo "<td>";
    echo "Categoria";
    echo "</td>";
    echo "</tr>";
    foreach ($result as $row) {
      echo "<tr>";
      echo "<td>";
      echo $row["nombre"];
      echo "</td>";

      echo "<td>";
      echo $row["precio"];
      echo "</td>";

      echo "<td>";
      echo $row["marca"];
      echo "</td>";

      echo "<td>";
      echo $row["empresa"];
      echo "</td>";

      echo "<td>";
      echo $row["categoria"];
      echo "</td>";
      echo "</tr>";

    }
    echo "</table>";


    $myPDO=null;
     ?>
  </body>
</html>
