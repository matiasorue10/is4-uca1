<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 3</title>
    <style>
      table{
        border: 1px solid black;
        border-collapse: collapse;
      }
      tr,td{
        border: 1px solid black;
      }
    </style>
  </head>
  <body>
    <?php
    try {
      $conn = pg_connect("host=localhost dbname=ejercicio1 user=postgres password=postgres");
      $query = "select p.id_producto as idproducto, p.nombre, p.precio, cat.nombre as categoria, m.nombre as marca, e.nombre as empresa
      from producto p
      join categoria cat on p.id_categoria = cat.id_categoria
      join marca m on m.id_marca = p.id_marca
      join empresa e on m.id_empresa = e.id_empresa";
      $result = pg_query($conn, $query);

      echo "<table>";
      echo "<tr>";
      echo "<td>";
      echo "Nombre";
      echo "</td>";
      echo "<td>";
      echo "Precio";
      echo "</td>";
      echo "<td>";
      echo "Marca";
      echo "</td>";
      echo "<td>";
      echo "Empresa";
      echo "</td>";
      echo "<td>";
      echo "Categoria";
      echo "</td>";
      echo "</tr>";
      $rows = pg_fetch_all($result);
      foreach ($rows as $row) {
        echo "<tr>";
        echo "<td>";
        echo $row['nombre'];
        echo "</td>";

        echo "<td>";
        echo $row['precio'];
        echo "</td>";

        echo "<td>";
        echo $row['categoria'];
        echo "</td>";

        echo "<td>";
        echo $row['marca'];
        echo "</td>";

        echo "<td>";
        echo $row['empresa'];
        echo "</td>";
        echo "</tr>";
      }
      echo "</table>";


      $myPDO=null;
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
     ?>
  </body>
</html>
