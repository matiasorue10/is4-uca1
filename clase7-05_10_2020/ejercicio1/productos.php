<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 1 - Productos</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script type="text/javascript" src="./productos.js"></script>
  </head>
  <body>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <?php
      require_once 'process.php';
       if(isset($_SESSION['message'])){
            echo "<div class='alert alert-{$_SESSION['msg_type']}'>";
            echo $_SESSION['message'];
            unset($_SESSION['message']);
            echo "</div>";
        } ?>
    </div>
  </div>
<div class="row">
<div class="col-md-12">
  <table class="table table-bordered table-hover">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Marca</th>
        <th>Tipo</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>


    <?php
      include('productosDb.php');

      $productos = new Productos();

      $data = $productos->getProductos();
      if($data){
        $dataLength = count($data);

        for ($i=0; $i < $dataLength ; $i++) {
          echo "<tr>";
          echo "<td>";
          echo $data[$i]['producto_id'];
          echo "</td>";
          echo "<td>";
          echo $data[$i]['nombre'];
          echo "</td>";
          echo "<td>";
          echo $data[$i]['descripcion'];
          echo "</td>";
          echo "<td>";
          echo $data[$i]['marca_nombre'];
          echo "</td>";
          echo "<td>";
          echo $data[$i]['nombre_tipo'];
          echo "</td>";
          echo "<td>";
          echo "  <span style='cursor: pointer;' onclick='borrarProducto(";
          echo $data[$i]['producto_id'];
          echo ")'>Borrar</span> &nbsp;";
          echo "  <span style='cursor: pointer;' onclick='editarProducto(";
          echo $data[$i]['producto_id'];
          echo ")'>Editar </span>";
          echo "</td>";
          echo "</tr>";
        }
      }
     ?>
     </tbody>
  </table>
</div>
</div>
<div class="row">
<div class="col-md-12">
  <form action="process.php" method="POST">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <div class="form-row">
      <div class="col-md-3">
        <div class="form-group position-relative">
          <label for="nombre">Nombre</label>
          <input type="text" name="nombre" value="<?php echo $nombre; ?>" class="form-control" id="nombre">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group position-relative">
          <label for="descripcion">Descripcion</label>
          <input type="text" name="descripcion" value="<?php echo $descripcion; ?>" class="form-control" id="descripcion">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group position-relative">
          <label for="marca">Marca</label>
          <select class="form-control" name="marca" id="marca">
            <option value="1" <?php if($marca == '1'): ?> selected="selected"<?php endif; ?> >Coca Cola</option>
            <option value="2" <?php if($marca == '2'): ?> selected="selected"<?php endif; ?> >Pechugon</option>
            <option value="3" <?php if($marca == '3'): ?> selected="selected"<?php endif; ?> >Ochi</option>
            <option value="4" <?php if($marca == '4'): ?> selected="selected"<?php endif; ?> >La Negrita</option>
            <option value="5" <?php if($marca == '5'): ?> selected="selected"<?php endif; ?> >Trebol</option>
            <option value="6" <?php if($marca == '6'): ?> selected="selected"<?php endif; ?> >Doña Angela</option>
            <option value="7" <?php if($marca == '7'): ?> selected="selected"<?php endif; ?> >Miller</option>
            <option value="8" <?php if($marca == '8'): ?> selected="selected"<?php endif; ?> >Heineken</option>
            <option value="9" <?php if($marca == '9'): ?> selected="selected"<?php endif; ?> >Pepsi</option>
            <option value="10" <?php if($marca == '10'): ?> selected="selected"<?php endif; ?> >Cervepar</option>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group position-relative">
          <label for="tipo">Tipo</label>
          <select class="form-control" name="tipo" id="tipo">
            <option value="1" <?php if($tipo == '1'): ?> selected="selected"<?php endif; ?> >Bebidas Nutritivas</option>
            <option value="2" <?php if($tipo == '2'): ?> selected="selected"<?php endif; ?> >Bebidas Alcoholicas</option>
            <option value="3" <?php if($tipo == '3'): ?> selected="selected"<?php endif; ?> >Gaseosas</option>
            <option value="4" <?php if($tipo == '4'): ?> selected="selected"<?php endif; ?> >Panificados</option>
            <option value="5" <?php if($tipo == '5'): ?> selected="selected"<?php endif; ?> >Verduras</option>
            <option value="6" <?php if($tipo == '6'): ?> selected="selected"<?php endif; ?> >Lacteos</option>
            <option value="7" <?php if($tipo == '7'): ?> selected="selected"<?php endif; ?> >Carnes</option>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <?php if($update== true): ?>
          <button type="submit" name="update" class="btn btn-primary">Editar</button>

      <?php else: ?>
        <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
      <?php endif; ?>
      </div>
    </div>
  </form>
</div>
</div>
</div>


  </body>
</html>
