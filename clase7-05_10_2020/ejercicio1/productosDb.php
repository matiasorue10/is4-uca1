<?php
  /**
   *
   */
  require_once('conexion.php');
  class Productos
  {

    function getProductos(){
      try {
        $db = new Conexion();
        $dbPDO = $db->Conectar();

        $query = "select pro.producto_id, pro.nombre, pro.descripcion,
                  pro.tipo_id, tipo.nombre as nombre_tipo,
                  pro.marca_id, marca.nombre as marca_nombre
                  from producto pro
                  join tipo on pro.tipo_id = tipo.tipo_id
                  join marca on pro.marca_id = marca.marca_id";
        $sql = $dbPDO->prepare($query);
        $sql->execute() or die(print_r($sql->errorInfo(), true));

        $row = $sql->fetchAll(PDO::FETCH_ASSOC);

        return($row);
      } catch (Exception $e) {
        return "Error:" .$e;
      }
    }

    function editarProducto($id, $producto){

    }

    function borrarProducto($id){

    }
  }

 ?>
