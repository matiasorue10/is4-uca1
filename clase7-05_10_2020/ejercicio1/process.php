<?php
  require_once('conexion.php');
  $id = 0;
  $descripcion = '';
  $marca = '';
  $tipo = '';
  $nombre = '';
  $update = false;
  session_start();
  /* CREANDO LA ENTIDAD */

  if (isset($_POST['guardar'])) {
    $nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    $marca = $_POST['marca'];
    $tipo = $_POST['tipo'];

    try {
      $db = new Conexion();
      $dbPDO = $db->Conectar();

      $query = "insert into producto (tipo_id, marca_id, nombre, descripcion) values ({$tipo},{$marca},'$nombre', '$descripcion');";
      $sql = $dbPDO->prepare($query);
      $sql->execute() or die(print_r($sql->errorInfo(), true));

      $_SESSION['message']= "Enhorabuena, se ha guardado el producto";
      $_SESSION['msg_type']= "success";

      header("Location: productos.php");

    } catch (Exception $e) {
      return "Error:" .$e;
    }
  }

  /* BORRANDO LA ENTIDAD */
  if(isset($_GET['delete'])){
    $id = $_GET['delete'];

    try {
      $db = new Conexion();
      $dbPDO = $db->Conectar();

      $query = "delete from producto where producto_id = $id;";
      $sql = $dbPDO->prepare($query);
      $sql->execute() or die(print_r($sql->errorInfo(), true));

      $_SESSION['message']= "Enhorabuena, se ha borrado el producto";
      $_SESSION['msg_type']= "danger";

      header("Location: productos.php");


    } catch (Exception $e) {
      return "Error:" .$e;
    }
  }

  if(isset($_GET['edit'])){
    $id = $_GET['edit'];
    $update = true;
    try {
      $db = new Conexion();
      $dbPDO = $db->Conectar();

      $query = "select * from producto where producto_id = $id;";
      $sql = $dbPDO->prepare($query);
      $sql->execute() or die(print_r($sql->errorInfo(), true));

      $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      if(count($result)==1){
        $id = $result[0]['producto_id'];
        $nombre = $result[0]['nombre'];
        $descripcion = $result[0]['descripcion'];
        $marca = $result[0]['marca_id'];
        $tipo = $result[0]['tipo_id'];
      }

      // header("Location: productos.php?edit={$id}");


    } catch (Exception $e) {
      return "Error:" .$e;
    }
  }

/* Editando la entidad*/
  if(isset($_POST['update'])){
    $id = $_POST['id'];
    $nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    $marca = $_POST['marca'];
    $tipo = $_POST['tipo'];

        try {
          $db = new Conexion();
          $dbPDO = $db->Conectar();
          $query = "update producto set tipo_id={$tipo}, marca_id={$marca}, nombre='$nombre', descripcion='$descripcion' where producto_id=$id;";
          $sql = $dbPDO->prepare($query);
          $sql->execute() or die(print_r($sql->errorInfo(), true));

          $_SESSION['message']= "Enhorabuena, se ha actualizado el producto";
          $_SESSION['msg_type']= "warning";

          header("Location: productos.php");

        } catch (Exception $e) {
          return "Error:" .$e;
        }
  }

 ?>
