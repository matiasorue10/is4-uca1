<?php

require_once('vendor/autoload.php');
use Ejercicio1\Persona;
use Ejercicio1\Empleado;

$empleado1 = new Empleado('Matias', 'Orue', '123456', 'Alguna dirección', '22', 'CODIGO1');
$empleado2 = new Empleado('Marco', 'Trovato', '123457', 'Alguna dirección', '25', 'CODIGO2');
$empleado3 = new Empleado('Miguel', 'Brunotte', '123458', 'Alguna dirección', '45', 'CODIGO3');
$empleado4 = new Empleado('Emmanuel', 'Adebayor', '123459', 'Alguna dirección', '38', 'CODIGO4');
$empleado5 = new Empleado('Pepe', 'Botella', '123460', 'Alguna dirección', '18', 'CODIGO5');

$empleado1->addTelefono('Celular', '0971 234 567');
$empleado1->setEmpresa('Club Olimpia', 'Mcal Lopez 1902');

$empleado2->addTelefono('Celular', '0971 234 568');
$empleado2->setEmpresa('Club Olimpia', 'Mcal Lopez 1902');

$empleado3->addTelefono('Celular', '0971 234 569');
$empleado3->setEmpresa('Club Olimpia', 'Mcal Lopez 1902');

$empleado4->addTelefono('Celular', '0971 234 570');
$empleado4->setEmpresa('Club Olimpia', 'Mcal Lopez 1902');

$empleado5->addTelefono('Celular', '0971 234 123');
$empleado5->setEmpresa('Google', 'Mcal Lopez 1902');

$empleados = array($empleado1, $empleado2, $empleado3, $empleado4, $empleado5);

foreach ($empleados as $empleado ) {
  echo "<br>{$empleado->__toString()} <br>";
  echo "{$empleado->getTelefonos()} <br>";
  echo "Codigo: {$empleado->getCodigo()} <br>";
  echo "Empresa: {$empleado->getEmpresa()} <br>";
}


 ?>
