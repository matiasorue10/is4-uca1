<?php
namespace Ejercicio1;
/**
 *
 */

require_once('telefono.php');
class Persona
{
  private $nombre;
  private $apellido;
  private $cedula;
  private $direccion;
  private $edad;

  private $telefonos = array();

  function __construct($nombre, $apellido, $cedula, $direccion, $edad)
  {
    $this->nombre = $nombre;
    $this->apellido = $apellido;
    $this->cedula = $cedula;
    $this->direccion = $direccion;
    $this->edad = $edad;
  }

  function getNombre(){
    return $this->nombre;
  }

  function setNombre($nombre){
    $this->nombre = $nombre;
  }

  function getApellido(){
    return $this->apellido;
  }

  function setApellido($apellido){
    $this->apellido = $apellido;
  }

  function getCedula(){
    return $this->cedula;
  }

  function setCedula($cedula){
    $this->cedula = $cedula;
  }

  function getDireccion(){
    return $this->direccion;
  }

  function setDireccion($direccion){
    $this->direccion = $direccion;
  }

  function getEdad(){
    return $this->edad;
  }

  function setEdad($direccion){
    $this->edad = $edad;
  }

  function __toString(){
    return "{$this->nombre} , {$this->apellido} Cedula: {$this->cedula} , Edad: {$this->edad}, Dirección: {$this->direccion}";
  }

  function addTelefono($tipo, $numero){
    $telefono = new Telefono($tipo,$numero);
    array_push($this->telefonos, $telefono);
  }

  function getTelefonos(){
    echo "Telefonos <br>";
    foreach ($this->telefonos as $telefono) {
      echo "{$telefono->getTipo()} : {$telefono->getNumero()}";
    }
  }
}

 ?>
