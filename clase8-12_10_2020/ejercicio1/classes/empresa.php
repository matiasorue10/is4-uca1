<?php
namespace Ejercicio1;

/**
 *
 */
class Empresa
{
  private $nombre;
  private $direccion;

  function __construct($nombre, $direccion)
  {
    $this->nombre = $nombre;
    $this->direccion = $direccion;
  }

  function getNombre(){
    return $this->nombre;
  }

  function setNombre($nombre){
    $this->nombre = $nombre;
  }

  function getDireccion(){
    return $this->direccion;
  }

  function setDireccion($direccion){
    $this->direccion = $direccion;
  }
}

 ?>
