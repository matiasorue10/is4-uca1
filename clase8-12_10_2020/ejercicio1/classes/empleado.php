<?php
namespace Ejercicio1;
/**
 *
 */
class Empleado extends Persona
{
  private $codigoEmpleado;
  private $empresa;

  function __construct($nombre, $apellido, $cedula, $direccion, $edad, $codigoEmpleado)
  {
    parent::__construct($nombre, $apellido,$cedula, $direccion, $edad);
    $this->codigoEmpleado = $codigoEmpleado;
  }

  function getCodigo(){
    return $this->codigoEmpleado;
  }

  function setCodigo($codigo){
     $this->codigoEmpleado = $codigo;
  }

  function setEmpresa($nombre, $direccion){
    $this->empresa = new Empresa($nombre, $direccion);
  }

  function getEmpresa(){
    if($this->empresa){
        $empresaData = "Nombre: {$this->empresa->getNombre()} Dirección: {$this->empresa->getDireccion()}";
    }
    return $empresaData;
  }
}

 ?>
