<?php
  /**
   *
   */
  namespace Ejercicio1;

  class Telefono
  {

    private $tipo;
    private $numero;

    function __construct($tipo, $numero)
    {
      $this->tipo = $tipo;
      $this->numero = $numero;
    }

    function getNumero(){
      return $this->numero;
    }

    function setNumero($numero){
      $this->numero = $numero;
    }

    function getTipo(){
      return $this->tipo;
    }

    function setTipo($tipo){
      $this->tipo = $tipo;
    }
  }

 ?>
