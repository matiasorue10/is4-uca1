<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 2</title>
</head>
<body>

<?php
  echo "PHP Version ";
  echo PHP_VERSION;
  echo "<br>";
  echo "ID PHP Version ";
  echo PHP_VERSION_ID;
  echo "<br>";
  echo "Valor máximo soportado para enteros ";
  echo PHP_INT_MAX;
  echo "<br>";
  echo "Tamaño máximo de nombre de un archivo ";
  echo PHP_INT_MAX;
  echo "<br>";
  echo "Versión del sistema operativo: ";
  echo PHP_OS;
  echo "<br>";
  echo "Símbolo correcto de Fin de Linea para la plataforma en uso:  ";
  echo PHP_EOL;
  echo "<br>";
  echo "El include path por defecto:  ";
  echo DEFAULT_INCLUDE_PATH;

?>

</body>
</html>