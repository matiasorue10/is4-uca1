<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 5</title>
</head>
<body>

<?php
  $form = <<<EOD
    <form>
        <label for="nombre">Nombre</label>
        <input type="text" id="apellido">
        <br>
        <br>
        <label for="apellido">Apellido</label>
        <input type="text" id="apellido">
        <br>
        <br>
        <label for="edad">Edad</label>
        <input type="text" id="edad">
        <br>
        <br>
        <button type="submit">Enviar</button>
    </form>
  EOD;

  echo $form;
?>

</body>
</html>