<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 4</title>
    <style>
    .bigger{
        color: green;
    }
    .smallest{
        color: red;
    }
    </style>
</head>
<body>

<?php

    $a = mt_rand(50,900);
    $b = mt_rand(50,900);
    $c = mt_rand(50,900);

    $values = array($a, $b, $c);
    sort($values, SORT_NUMERIC);

    $length = count($values);

    for($i = 0; $i < $length; $i++){
        if($i == 0){
            echo "<span class='smallest'>";
            echo $values[$i];
            echo "</span>";
        } else if ($i == 2  ){
            echo "<span class='bigger'>";
            echo $values[$i];
            echo "</span>";
        } else {
            echo "<span>";
            echo $values[$i];
            echo "</span>";
        }
        echo "<span> &nbsp;";
        echo "</span>";
        
    }

?>

</body>
</html>