<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 3</title>
    <style>
     table{
        border-collapse: collapse;
     }
    td{
            border: 1px solid black;
    }    
    </style>
</head>
<body>

<table>
<tbody>

<?php
    $n = 10;
    echo "<tr>";
    echo "<td>";
    echo "N = $n";
    echo "</td>";
    echo "</tr>";

    for($i = 1; $i <= $n; $i++){
        if($i % 2 == 0){
            echo "<tr>";
            echo "<td>";
            echo "$i";
            echo "</td>";
            echo "</tr>";

        }
    }

?>

</tbody>

</table>
</body>
</html>