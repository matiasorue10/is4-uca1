<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi primer Script PHP - Matias Orue</title>
    <link rel="stylesheet" href="./styles/style.css">
</head>

<body>
    <table>
        <thead>
            <th colspan="3">Productos</th>
        </thead>
    <tbody>
    <tr class="secondary-thead">
        <td>Nombre</td>
        <td>Cantidad</td>
        <td>Precio (Gs)</td>
    </tr>
    
    <?php 
        require_once "Data.php";
        $d = new Data();
        $lista = $d->getGaseosas();

        foreach ($lista as $gaseosa) {
            if($gaseosa->id%2!=0){
                echo "<tr>";
            } else {
                echo "<tr class='secondary-thead'>";
            }
            echo "<td>";
            echo $gaseosa->nombre;
            echo "</td>";
            echo "<td>";
            echo $gaseosa->cantidad;
            echo "</td>";
            echo "<td>";
            echo $gaseosa->precio;
            echo "</td>";
            echo "</tr>";
        };
    ?>
    </tbody>
    </table>
    <p>
        Hecho por: Matias Orué C. (CO6431)
    </p>
</body>

</html>