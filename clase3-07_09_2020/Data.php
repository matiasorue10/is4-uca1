<?php 
    require_once "Gaseosa.php";
    class Data {
        
        function getGaseosas() {
            $gaseosas = array();

            $g = new Gaseosa('1', 'Coca Cola', '100', '4.500');
            array_push($gaseosas, $g);

            $g = new Gaseosa('2', 'Pepsi', '30', '4.800');
            array_push($gaseosas, $g);

            $g = new Gaseosa('3','Sprite', '20', '4.500');
            array_push($gaseosas, $g);

            $g = new Gaseosa('4', 'Guaraná', '200', '4.500');
            array_push($gaseosas, $g);

            $g = new Gaseosa('5', 'SevenUp', '24', '4.800');
            array_push($gaseosas, $g);

            $g = new Gaseosa('6','Mirinda Naranja', '56', '4.800');
            array_push($gaseosas, $g);

            $g = new Gaseosa('7', 'Mirinda Guaraná', '89', '4.800');
            array_push($gaseosas, $g);

            $g = new Gaseosa('8','Fanta Naranja', '10', '4.500');
            array_push($gaseosas, $g);

            $g = new Gaseosa('9','Fanta Piña', '2', '4.500');
            array_push($gaseosas, $g);
            return $gaseosas;
        }
    }
?>