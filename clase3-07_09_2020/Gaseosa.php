<?php
class Gaseosa
{
    public $id;
    public $nombre;
    public $cantidad;
    public $precio;

    public function __construct($id, $nombre, $cantidad, $precio)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->cantidad = $cantidad;
        $this->precio = $precio;
    }
}
?>