<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 4</title>
</head>
<body>

<?php
    $var = 24.5;
    echo gettype($var). "<br>";
    $var = 'HOLA';
    echo gettype($var);
    $var = 1902;
    echo var_dump($var);
?>

</body>
</html>
