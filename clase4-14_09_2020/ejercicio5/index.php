<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 5</title>
    <style>
        table{
            border-collapse: collapse;
        }
        td{
            border: 1px solid black;
        }
        tr.colored{
            background-color: gainsboro;
        }
    </style>
</head>
<body>
<table>
    <thead>
    <tr><td>Tabla del 9</td></tr>
    </thead>

<tbody>
    <tr>
        <?php
        $number = 9;
        for($i = 1; $i <= 10; $i++){
            if($i%2!=0){
                echo "<tr class='colored'>";
            } else {
                echo "<tr>";
            }
            echo "<td>";
            echo $i * 9;
            echo "</td>";
            echo "</tr>";
        }
        ?>
    </tr>
</tbody>
</table>
</body>
</html>
