<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 7</title>
</head>
<body>

<?php
    $parcial1 = rand(0,30);
    $parcial2 = rand(0,20);
    $final = rand(0,50);

    $total = $parcial1 + $parcial2 + $final;
    
    switch(true){
        case ($total <= 59):
            echo "Nota: 1 - Total de puntos $total";
            break;
        case ($total >= 60 && $total <= 69):
            echo "Nota: 2 - Total de puntos $total";
            break;
        case ($total >= 70 && $total <= 79):
            echo "Nota: 3 - Total de puntos $total";
            break;
        case ($total >= 80 && $total <= 89):
            echo "Nota: 4 - Total de puntos $total";
            break;
        case ($total >= 90):
            echo "Nota: 5 - Total de puntos $total";
            break;
    }

?>

</body>
</html>