<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 8</title>
</head>
<body>

<?php

    $i = 0;
    while($i != 900){
        $a = rand(1,10000);
        if($a % 2 == 0){
            $i = $i +1;
            echo "$a";
            echo "<br>";    
        }
    }

    echo "Total numeros -> $i";

?>

</body>
</html>