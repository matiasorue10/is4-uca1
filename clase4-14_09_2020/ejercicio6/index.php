<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 6</title>
</head>
<body>

<?php
    $a = rand(99,999);
    $b = rand(99,999);
    $c = rand(99,999);

    $var1 = $a*3;
    $var2 = $b+$c;
    if($var1 > $var2){
        echo "La expresión a * 3 ( $var1 ) es mayor que b+c ( $var2 )";
    } else {
        echo "La expresión b+c ( $var2 ) es mayor que a * 3 ( $var1 )";
    }

?>

</body>
</html>